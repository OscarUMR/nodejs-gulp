// 1.definir tarea para minificar imagen con el plugin gulp-imagemin
// 2. definir tarea para minificar imagen con el plugin gulp-clean-css, gulp-autoprefixer
// 3. definir tarea para minificar imagen con el plugin gulp-htmlmin
// 4. definir tarea para minificar imagen con el plugin gulp-uglify
var gulp = require( 'gulp' );
var images = require( 'gulp-imagemin' );
var html = require( 'gulp-htmlmin' );
var clean = require('gulp-clean-css');
var autoprefixer = require('gulp-autoprefixer');
var uglify = require('gulp-uglify');



gulp.task('minificar-imagenmin', function() {
  gulp.src('src/images/*')
    .pipe(images())
    .pipe( gulp.dest('build/images'));
});

gulp.task('minificar-css', function() {
  gulp.src('src/css/*')
    .pipe(clean())
    .pipe(autoprefixer())
    .pipe( gulp.dest('build/css'));
});

gulp.task('minificar-htmlmin', function() {
  gulp.src('src/html/*')
    .pipe(html())
    .pipe(gulp.dest('build/html'));
});

gulp.task('minificar-uglify', function() {
  gulp.src('src/js/*')
    .pipe(uglify())
    .pipe(gulp.dest('build/js'));
});
